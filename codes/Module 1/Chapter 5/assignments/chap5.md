# __*<u>Hierarchical Transformations: 3D Articulated Arm</u>*__
<br><br>
<!-- ('#' is used for headings '*' for italics '__' for bold 'u' for underline '<br>' for breakline '>' for block quotes '>>' for nested block quotes '-' for unordered list)-->
> ### <u>AIM</u>
>> Objective of this experiment is to understand the effective tranformation due to a series of transformations and how to construct a hierarchical model.

> ### <u>THEORY</u>
>> Here in this experiment we put the concepts of multiple transformations learnt in experiment 5a to practice with an Articulated arm experiment.<br><br>
A hierarchy is an organization of things into levels. In a very general sense, he things higher up in a hierachy have control over the lower things. Here this applies to the matrix transformations.
We constructed a hierarchical articulated arm model where each node/part follows a series of transformations from the root node to its current position. The top-most object in this hierarchy is the Shoulder followed by Elbow, Forearm, Wrist and Palm. Moving the shoulder moves the rest of the parts as all others derive their transformation from the shoulder. When you move the wrist only the palm moves while the shoulder, elbow and forearm remain unchanged. This is because the wrist is lower in hierarchy than your elbow and therefore has no control on it.

> ### <u>PROCEDURE</u>
>> This experiment introduces the concept of the hierarchical transformations and a hierarchical model.
>>- The hierarchical articulated arm is provided.
>>- Notice the different nodes used to construct the model.
>>- Nodes in this model follows the hierarchy: Shoulder (highest), Elbow, Forearm, Wrist and Palm.
>>- Applying transformations to a node in the hierarchy applies the same transformations to all the lower nodes in the hierarchy in addition to their own transformations. For example, a rotation has been applied to the shoulder.
>>- You drag the slider you can notice this rotation being applied to the other nodes toother nodes.
>>- Try modifying the transformations in the hierarchy and notice the effective changes on the entire model.
>>- The transormation nodes of Forearm and Palm and notice how a Cube has been moulded to a forearm and a palm using the respective transformations.