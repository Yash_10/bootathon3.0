function checkarea(){
    let x:HTMLInputElement=<HTMLInputElement>document.getElementById("x");
    let y:HTMLInputElement=<HTMLInputElement>document.getElementById("y");

    let x1:HTMLInputElement=<HTMLInputElement>document.getElementById("x1");
    let y1:HTMLInputElement=<HTMLInputElement>document.getElementById("y1");
    let x2:HTMLInputElement=<HTMLInputElement>document.getElementById("x2");
    let y2:HTMLInputElement=<HTMLInputElement>document.getElementById("y2");
    let x3:HTMLInputElement=<HTMLInputElement>document.getElementById("x3");
    let y3:HTMLInputElement=<HTMLInputElement>document.getElementById("y3");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    let ans1:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans1");
    var vx:number=parseFloat(x.value);
    var vy:number=parseFloat(y.value);
    var vx1:number=parseFloat(x1.value);
    var vy1:number=parseFloat(y1.value);
    var vx2:number=parseFloat(x2.value);
    var vy2:number=parseFloat(y2.value);
    var vx3:number=parseFloat(x3.value);
    var vy3:number=parseFloat(y3.value);

//first we find the area of original triangle
    var area:number=Math.abs(vx1*(vy2-vy3)+vx2*(vy3-vy1)+vx3*(vy1-vy2))/2;
    ans.innerHTML="The area of Triangle :"+area.toString();
//now we find the areas of 3 triangles formed by the new point 
    var area1:number=Math.abs(vx*(vy1-vy2)+vx1*(vy2-vy)+vx2*(vy-vy1))/2;
    var area2:number=Math.abs(vx*(vy2-vy3)+vx2*(vy2-vy3)+vx3*(vy-vy2))/2;
    var area3:number=Math.abs(vx*(vy3-vy1)+vx1*(vy-vy3)+vx3*(vy1-vy))/2;
//we add the newly found areas of 3 triangles
    var sum:number=area1+area2+area3;

    if(Math.abs(area-sum)<0.000001){
        ans1.innerHTML="The point lies inside the triangle";
    }
    else{
        ans1.innerHTML="The point lies outside the triangle";
    }

}