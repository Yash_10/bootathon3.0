function check() {
    var a = document.getElementById("num");
    var num = +a.value;
    if (isNaN(num)) {
        alert("It is not a number it is a string");
    }
    else {
        if (num % 2 == 0)
            alert("It is an even number");
        else
            alert("It is an odd number");
    }
}
//# sourceMappingURL=evenodd.js.map