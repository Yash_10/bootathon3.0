function seperate(){
    var p1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var p2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var p3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");

    var complex:string = p1.value;
    var real:number;
    var imaginary:number;
    var i:number=complex.indexOf("+");

    if(i!=-1){
        real=+complex.substring(0,i);
        imaginary=+complex.substring(i+1,complex.length-1);
        p2.value="The real part of complex number :"+real;
        p3.value="The img part of complex number  :"+imaginary;
    }
    else{
        real =+complex.substring(0,complex.length);
        p2.value="The real part of complex number :"+real;
        p3.value="The img part of complex number :0";
    }

}