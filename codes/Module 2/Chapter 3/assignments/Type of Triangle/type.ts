function check(){
    var x:HTMLInputElement=<HTMLInputElement>document.getElementById("x");
    var y:HTMLInputElement=<HTMLInputElement>document.getElementById("y");
    var z:HTMLInputElement=<HTMLInputElement>document.getElementById("z");

    var a:number= +x.value;
    var b:number= +y.value;
    var c:number= +z.value;

    if(a==b && b==c){
        document.getElementById("op").innerHTML="The triangle is an equilateral triangle";
    }
    else if((a==b && a!=c) ||(b==c && b!=a) || (a==c && a!=b)){
        document.getElementById("op").innerHTML="The triangle is an isosceles triangle";
    }
    else if(((a==b && a!=c) ||(b==c && b!=a) || (a==c && a!=b)) && (Math.pow(a,2)==Math.pow(b,2)+Math.pow(c,2) || Math.pow(b,2)==Math.pow(a,2)+Math.pow(c,2) || Math.pow(c,2)==Math.pow(b,2)+Math.pow(a,2))){
        document.getElementById("op").innerHTML="The triangle is an isosceles right angled triangle";
    }
    else if((a!=b && b!=c && a!=c) && (Math.pow(a,2)==Math.pow(b,2)+Math.pow(c,2) || Math.pow(b,2)==Math.pow(a,2)+Math.pow(c,2) || Math.pow(c,2)==Math.pow(b,2)+Math.pow(a,2))){
        document.getElementById("op").innerHTML="The triangle is a scalene right angled triangle";
    }
    else if(a!=b && b!=c && a!=c){
        document.getElementById("op").innerHTML="The triangle is a scalene triangle";
    }
}