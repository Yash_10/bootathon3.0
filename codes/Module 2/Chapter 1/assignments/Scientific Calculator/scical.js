//declare all the variables used in the HTML file
var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function div() {
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();
}
function sin() {
    var c = Math.sin(parseFloat(t1.value));
    t3.value = c.toString();
}
function cos() {
    var c = Math.cos(parseFloat(t1.value));
    t3.value = c.toString();
}
function tan() {
    var c = Math.tan(parseFloat(t1.value));
    t3.value = c.toString();
}
function sqrt() {
    var c = Math.sqrt(parseFloat(t1.value));
    t3.value = c.toString();
}
function pow() {
    var c = Math.pow(parseFloat(t1.value), parseFloat(t2.value));
    t3.value = c.toString();
}
//# sourceMappingURL=scical.js.map