function area(){
    let x1:HTMLInputElement=<HTMLInputElement>document.getElementById("x1");
    let y1:HTMLInputElement=<HTMLInputElement>document.getElementById("y1");
    let x2:HTMLInputElement=<HTMLInputElement>document.getElementById("x2");
    let y2:HTMLInputElement=<HTMLInputElement>document.getElementById("y2");
    let x3:HTMLInputElement=<HTMLInputElement>document.getElementById("x3");
    let y3:HTMLInputElement=<HTMLInputElement>document.getElementById("y3");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var vx1:number=parseFloat(x1.value);
    var vy1:number=parseFloat(y1.value);
    var vx2:number=parseFloat(x2.value);
    var vy2:number=parseFloat(y2.value);
    var vx3:number=parseFloat(x3.value);
    var vy3:number=parseFloat(y3.value);


    //Using Herons Formula to find the area of triangle

    var a:number=Math.sqrt(Math.pow((vx2-vx1),2)+Math.pow((vy2-vy1),2));
    console.log(a);
    var b:number=Math.sqrt(Math.pow((vx1-vx3),2)+Math.pow((vy1-vy3),2));
    console.log(b);
    var c:number=Math.sqrt(Math.pow((vx3-vx2),2)+Math.pow((vy3-vy2),2));
    console.log(c);

    var s:number=(a+b+c)/2;
    var area:number=Math.sqrt(s*(s-a)*(s-b)*(s-c));
    //Displaying the final answer
    ans.innerHTML="The area of Triangle :"+area.toString();
}