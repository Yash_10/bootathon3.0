function arm() {
    var para = document.getElementById("para");
    var a;
    para.innerHTML = "Armstrong numbers between 100 and 999 are :";
    for (a = 100; a <= 999; a++) {
        var b = a;
        var sum = 0;
        while (b > 0) {
            var l = b % 10;
            sum = sum + (l * l * l);
            b = Math.floor(b / 10);
        }
        if (sum == a) {
            para.innerHTML = para.innerHTML + "<br>>  " + a.toString();
        }
    }
}
//# sourceMappingURL=arm_script.js.map