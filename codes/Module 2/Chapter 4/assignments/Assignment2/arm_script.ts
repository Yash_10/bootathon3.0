function arm() : void   {

    var para:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("para");

    var a:number;
    para.innerHTML = "Armstrong numbers between 100 and 999 are :";
    for(a=100;a <= 999; a++)  {
        var b:number = a;
        
        var sum:number = 0;
        while(b > 0)    {
            var l: number = b%10;
            sum = sum + (l * l * l);
            b = Math.floor(b / 10);
        }
        //to display the output
        if( sum == a )  {
            para.innerHTML = para.innerHTML + "<br>>  " + a.toString();
        }
    }
    
}