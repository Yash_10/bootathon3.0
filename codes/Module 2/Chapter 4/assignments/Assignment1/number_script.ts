function number_input(): void   {

    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("input");
    var para:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("para");

    var total:number = +input.value;
    var count,pos_count:number = 0,neg_count:number = 0,zero_count:number = 0;

    var array : number[] = new Array(total);
    //this for loop accepts input from the user
    for(count=0 ; count < total ; count++)    {

        array[count] = +prompt("Enter number "+(count+1));

    }
     
    for(count=0 ; count < total ; count++)    {
        //this counts the positive numbers entered by the user
        if(array[count] > 0)    {
            pos_count++;
        }
        //this counts the negative numbers entered by the user
        else if(array[count] < 0)    {
            neg_count++;
        }
        //this counts the zeros entered by the user
        else    {
            zero_count++;
        }

    }

    para.innerHTML = "Total count of positive numbers : "+pos_count+"<br>"+"Total count of negative numbers : "+neg_count+"<br>"+"Total count of zeros : "+zero_count;

}